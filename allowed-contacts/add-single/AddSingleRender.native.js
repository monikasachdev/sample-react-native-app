'use strict';
import React from 'react';
import { View, Modal, TouchableWithoutFeedback} from 'react-native';
import { Button, Text, Icon, Spinner ,InputGroup, Input} from 'native-base';
const dismissKeyboard = require('dismissKeyboard');
import styles from '../../../../www/css/style';

export default function () {
    return AddSingle.call(this);
}

var AddSingle = function () {
    return (
        <Modal visible={this.props.showAddSingle} onRequestClose={this.props.closeModal.bind(this,'inner')}
               animationType={'fade'} transparent={true}>
            <TouchableWithoutFeedback onPress={()=>{dismissKeyboard()}}>
                <View style={[styles.container, styles.opacity100]}>
                    <View style={[styles.alignCenter]}>
                        <Text style={[styles.heading, styles.textCenter, styles.width300]}>
                            Enter the email you would like to receive notifications for:
                        </Text>
                        {
                            //<InputGroup borderType='underline'>
                            //    <Input autoFocus={true} onChangeText={(e) => {this.setState({name:e})}}
                            //           autoCapitalize={'none'} placeholder='Enter Name'/>
                            //</InputGroup>
                        }
                        <InputGroup borderType='underline' style={styles.textfield}>
                            <Icon name="md-person" style={styles.icon} />
                            <Input autoFocus={true} keyboardType="email-address"
                                   onChangeText={(e) => {this.setState({email:e})}}
                                   returnKeyType={'done'}
                                   onSubmitEditing={this.addSingle }
                                   autoCapitalize={'none'} placeholder='Enter Email'/>
                        </InputGroup>
                        <View style={[styles.btns,{marginTop :20, justifyContent:'center'}]}>
                            <Button block style={styles.btn} onPress={this.addSingle} iconRight><Icon
                                name='md-checkmark-circle-outline'/>Submit</Button>
                            <Button style={styles.btn}
                                    onPress={this.props.closeModal.bind(this,'inner')}
                                    iconRight>back <Icon
                                name='md-arrow-round-back'/></Button>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    )
}
