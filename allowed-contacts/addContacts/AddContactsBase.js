'use strict';
import React,{Component} from 'react';
import lodash from 'lodash';
import validator from 'validator';
import api from '../../../common/api';
import {detect, notify, storage, navigate} from '../../../common/react-cross-platform';
import * as redux from '../../../redux/redux';

class AddContacts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            loading: true,
            showSearchBox: false,
            showAddSingle: false,
            showFirstVisit: false,
            shouldCloseModal: false,
            contacts: [],
            total_contacts: 0,
            skip: 0,
            pageLimit: 50,
            currentEmailIndex: 0,
            currentEmail: '',
            searchText: '',
            emailAccounts: [],
            name: '',
            email: ''
        };
        this.selectedList = [];

        this.selected = this.selected.bind(this);
        this.saveContacts = this.saveContacts.bind(this);
        this.getContacts = this.getContacts.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.addSingle = this.addSingle.bind(this);
        this.mergeContacts = this.mergeContacts.bind(this);
        this.sortArray = this.sortArray.bind(this);
        this.filterContacts = this.filterContacts.bind(this);
        this.appendContacts = this.appendContacts.bind(this);
        this.toggleSearch = this.toggleSearch.bind(this);
    }

    closeModal(state) {
        switch (state) {
            case 'inner' :
                this.setState({showAddSingle: false});
                break;
            case 'firstVisit' :
                navigate('allowed-contacts', this.props);
                break;
            default :
                navigate('allowed-contacts', this.props);
                break;
        }
    }

    componentWillMount() {
        redux.getUserAsync().then((user) => {
            if (user && Array.isArray(user.emailAccounts) && user.emailAccounts.length > 0) {
                let contacts = redux.getContacts();

                //if (contacts) {
                let {allowedContacts, emailAccounts} = user;
                let {query} = this.props.location || '';
                let email = query && query.email ? query.email : this.props.email;
                let index = lodash.findIndex(emailAccounts, {email: email});
                let account = lodash.find(emailAccounts, {email: email});

                if (account) {
                    this.selectedList = allowedContacts;
                    contacts = this.mergeContacts(contacts, allowedContacts);
                    contacts = this.sortArray(contacts);
                    let total_contacts = contacts.length;
                    redux.setContacts(contacts);
                    contacts = contacts.slice(this.state.skip, this.state.skip + this.state.pageLimit);
                    this.setState({
                        open: true,
                        emailAccounts: emailAccounts,
                        currentEmail: email,
                        currentEmailIndex: index,
                        total_contacts,
                        shouldCloseModal: allowedContacts.length > 0,
                        contacts: contacts,
                        loading: false
                    });
                } else {
                    notify('please provide a valid email.');
                    navigate('detect-contacts', this.props, {query: {email: user.emailAccounts[0].email}});
                }
                //} else {
                //    let {email} = this.props.location.query;
                //    navigate('detect-contacts', this.props, {query: {email: email || user.emailAccounts[0].email}});
                //}
            } else {
                navigate('welcome-wizard', this.props);
            }
            return null;
        }).catch(err => {
            console.log('error in add contacts', err);
            navigate('login', this.props);
        });
    }

    toggleSearch() {
        let contacts = redux.getContacts().slice(0, this.state.pageLimit);
        this.setState({searchText: '', showSearchBox: !this.state.showSearchBox, contacts}, ()=> {
            detect.web ? this.refs.searchText.focus() : '';
        });
    }


    mergeContacts(contacts = [], allowedcontacts = []) {
        return lodash.unionBy(contacts, allowedcontacts, 'email');
    }

    sortArray(contacts = []) {
        let nameSort = (item) => {
            return item.name ? item.name.toLowerCase() : '';
        };
        let emailSort = (item) => {
            return item.email.toLowerCase()
        };
        return lodash.sortBy(contacts, [nameSort, emailSort]);
    }

    getContacts(index) {
        let email = this.state.emailAccounts[index].email;
        navigate('detect-contacts', this.props, {query: {email}});
    }

    getFilteredContacts(searchText = '') {
        let regex = new RegExp(searchText.trim(), 'i');
        let contacts = redux.getContacts();

        return contacts.filter((item) => {
            return (item.name ? item.email.match(regex) || item.name.match(regex) : '');
        });

    }

    /* function to call on serach...*/
    filterContacts(searchText) {
        let contacts = this.getFilteredContacts(searchText);

        contacts = contacts.slice(0, this.state.pageLimit);
        this.setState({contacts, searchText}, ()=> {
            detect.web ?
                $('#listOfContacts').scrollTop(0)
                : this.listOfContacts.scrollTo({x: 0, y: 0, animated: true});
        });
    }

    appendContacts(evt) {
        let contactsContainer = detect.web ? $('#listOfContacts') : this.listOfContacts;

        if (this.state.contacts.length < this.state.total_contacts) {
            if (detect.native) {
                let scrollBar = evt.nativeEvent.contentOffset.y;
                let contentheight = evt.nativeEvent.contentSize.height;

                contactsContainer._scrollViewRef.measure((scrollViewX, scrollViewY, scrollViewWidth, scrollViewHeight) => {
                    if (contentheight - scrollViewHeight - scrollBar > -1 && contentheight - scrollViewHeight - scrollBar < 1) {
                        console.log("in append contcts native");

                        let contacts = this.getFilteredContacts(this.state.searchText);

                        contacts = contacts.slice(0, this.state.contacts.length + this.state.pageLimit);
                        this.setState({contacts});
                    }
                });
            } else {
                if (contactsContainer[0].scrollHeight - contactsContainer.innerHeight() == contactsContainer.scrollTop()) {
                    console.log("in append contcts web");
                    let contacts = this.getFilteredContacts(this.state.searchText);

                    contacts = contacts.slice(0, this.state.contacts.length + this.state.pageLimit);
                    this.setState({contacts});
                }
            }
        }

    }

//nextContacts() {
//    if (Math.ceil(this.state.skip / this.state.pageLimit) + 1 == Math.ceil(this.state.total_contacts / this.state.pageLimit)) {
//        notify('no more contacts for this email');
//    } else if (Math.floor(this.state.skip / this.state.pageLimit) < Math.ceil(this.state.total_contacts / this.state.pageLimit)) {
//        let skip = this.state.skip + this.state.pageLimit;
//        let contacts = redux.getContacts();
//
//        contacts = contacts.slice(skip, skip + this.state.pageLimit);
//        this.setState({contacts, skip});
//    }
//}

//prevContacts() {
//    if (Math.floor(this.state.skip / this.state.pageLimit) >= 1) {
//        let skip = this.state.skip - this.state.pageLimit;
//        let contacts = redux.getContacts();
//
//        contacts = contacts.slice(skip, skip + this.state.pageLimit);
//        this.setState({contacts, skip});
//    }
//}

    selected(contact) {
        if (lodash.findIndex(this.selectedList, {email: contact.email}) >= 0) {
            this.selectedList.splice(_.findIndex(this.selectedList, {email: contact.email}), 1);
        } else {
            this.selectedList.push(contact);
        }
        this.forceUpdate();
    }


    addSingle(data) {
        let { email} = data;
        //name = name.trim();
        email = email.trim().toLowerCase();

        if (!email) {
            notify('Please fill all fields.');
        } else if (!validator.isEmail(email)) {
            notify('Please provide valid email-account.');
        } else if (lodash.find(this.selectedList, {email})) {
            notify('Account has already been added.');
        } else {
            this.selectedList.push({email});

            api.post('/data/users/addContacts', this.selectedList).then(result => {
                if (result.user) {

                    let contacts = redux.getContacts();
                    contacts = this.mergeContacts(contacts, this.selectedList);
                    contacts = this.sortArray(contacts);

                    redux.setContacts(contacts);
                    redux.setUser(result.user);
                    //pagination
                    contacts = contacts.slice(this.state.skip, this.state.skip + this.state.pageLimit);
                    this.setState({contacts, showAddSingle: false, shouldCloseModal: true}, ()=> {
                        storage.get('firstVisit').then(firstVisit => {
                            if (firstVisit) {
                                this.setState({showFirstVisit: true}, ()=> {
                                    storage.remove('firstVisit');
                                });
                            }
                        });
                    });
                } else {
                    notify('Nothing saved.');
                }
                return null;
            }).catch(err => console.log('error in addSingle', err));
        }
    }

    saveContacts() {
        api.post('/data/users/addContacts', this.selectedList).then(result => {
            if (result.user) {
                notify(result.notify);
                redux.setUser(result.user);
                storage.get('firstVisit').then(firstVisit => {
                    if (firstVisit) {
                        this.setState({showFirstVisit: true}, ()=> {
                            storage.remove('firstVisit');
                        });
                    } else {
                        this.closeModal();
                    }
                });
            } else {
                notify('Nothing saved.');
            }
            return null;
        }).catch(err => console.log('error in saveContacts', err));
    }
}

module.exports = AddContacts;
