'use strict';
import Base  from './AddContactsBase';
import Render  from './AddContactsRender';
import { connect } from 'react-redux'

class AddContacts extends Base {
    render() {
        return Render.call(this, this.props, this.state);
    }
}

export default AddContacts = connect((state)=> {
    return {contacts: state.contacts || [], user: state.user || {emailAccounts: []}}
})(AddContacts);
