import React from 'react';
import { View } from 'react-native';
import { Button, Icon, List, ListItem, Text,InputGroup,Input  } from 'native-base';

import AddSingle from './add-single/AddSingle';
import Loading from '../../subcomponents/loading/Loading';
import styles from '../../../www/css/style';

export default function () {
    return AllowedContactsComponentRender.call(this);
}

var AllowedContactsComponentRender = function () {
    return (
        <View style={{flex:1,margin:5}}>
            <Loading show={this.state.loading}/>
            <Text style={[styles.heading,styles.headingColor, styles.m_t_5]}>
                - Add contacts to this list to receive notifications from them.
            </Text>
            <View style={[styles.btns,styles.m_b_3]}>
                <Button small iconRight style={styles.btn} onPress={this.addContact}><Icon name="md-person-add"/>Auto
                    detect</Button>
                <Button small iconRight style={[styles.btn, styles.m_l_10]}
                        onPress={()=>{ this.setState({showAddSingle:true}) } }>
                    <Icon name="md-person-add"/>Add Single
                </Button>
                <Button small iconRight style={[styles.btn, styles.m_l_10]} onPress={this.toggleSearchBox}>
                    <Icon name="ios-search"/>Search
                </Button>
            </View>
            {/*showing serach box here via toggle ....*/}
            {this.state.showSearchBox ?
                <InputGroup borderType='underline'>
                    <Icon name="ios-search" style={styles.icon}/>
                    <Input autoFocus={true} onChangeText={e => { this.filerAllowedContacts(e) }}
                           autoCapitalize={'none'} value={this.state.searchText} placeholder="Search..."/>
                </InputGroup>
                : null}
            {/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/}
            <List style={[styles.m_t_2]}>
                <ListItem itemDivider>
                    <Text>Contacts</Text>
                </ListItem>
            </List>
            <List dataArray={this.state.allowedContacts}
                  renderRow={(item) =>
              <ListItem>
                <View style={[styles.flexRow,styles.justifySpace]}>
                    <View style={[styles.justifyCenter, styles.flex8]}>
                        <Text style={styles.font14} ellipsizeMode={'tail'} numberOfLines={1}>
                            {item.email}
                        </Text>
                        {
                            //<Text style={[styles.l20]}>{item.name}</Text>
                        }
                    </View>
                    <Button transparent style={[styles.flex1, styles.justifyEnd]} onPress={this.removeContact.bind(this, item)}>
                        <Icon name="md-trash" style={styles.icon} />
                    </Button>
                </View>
              </ListItem>
            }>
            </List>

            <AddSingle showAddSingle={this.state.showAddSingle} addSingle={this.addSingle}
                       closeModal={this.closeModal}/>
        </View>
    );
}
