'use strict';
import Base  from './AllowedContactsBase';
import Render  from './AllowedContactsRender';
import { connect } from 'react-redux'

class AllowedContacts extends Base {
    render() {
        return Render.call(this, this.props, this.state);
    }
}
export default AllowedContacts;
